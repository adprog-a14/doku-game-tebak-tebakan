# Game Tebakan

[![pipeline status](https://gitlab.com/adprog-a14/doku/badges/Adil-GameTebakan/pipeline.svg)](https://gitlab.com/adprog-a14/doku/-/commits/Adil-GameTebakan)
[![coverage report](https://gitlab.com/adprog-a14/doku/badges/Adil-GameTebakan/coverage.svg)](https://gitlab.com/adprog-a14/doku/-/commits/Adil-GameTebakan)

### Fungsi
Untuk membuat tebak-tebakan pada Aplikasi Doku dan mengecek jawaban user terhadap tebak-tebakan tersebut.

### URL:
https://doku-game-tebakan.herokuapp.com/

### Penggunaan:
- Generate Indonesia Riddle: (`https://doku-game-tebakan.herokuapp.com/game-tebakan?language=Indonesia`)
- Generate English Riddle: (`https://doku-game-tebakan.herokuapp.com/game-tebakan?language=English`)
- Check Answer (post method): (`https://doku-game-tebakan.herokuapp.com/game-tebakan/cek?encryptedAnswer=[see 'encryptedAnswer' in json data after generating riddle]&answer=[user answer]`)