package com.doku.core.randomwordgenerator.english.database;

import com.doku.core.randomwordgenerator.english.EnglishRandomWordGenerator;

/**
 * Hanya untuk ilustrasi maintenance.
 */
public class EnglishDatabase implements EnglishRandomWordGenerator {
    @Override
    public String generate() {
        return null;
    }
}
