package com.doku.core.randomwordgenerator.english.website;

import com.doku.core.randomwordgenerator.english.EnglishRandomWordGenerator;
import com.doku.core.riddlegenerator.FailRetrievingRiddleException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MerriamWebsterCom implements EnglishRandomWordGenerator {

    /**
     * Memilih kata untuk tebak-tebakan secara random.
     * @return kata tebak-tebakan
     * @throws FailRetrievingRiddleException jika terjadi kesalahan pada saat mengambil data
     */
    @Override
    public String generate() throws FailRetrievingRiddleException {
        String result = null;
        try {
            String meriamWebsterPage = randomMeriamWebsterPage();
            String meriamWebsterHtml = getMeriamWebsterHtml(meriamWebsterPage);
            result = getRandomWord(meriamWebsterHtml);
        } catch (IOException e) {
            throw new FailRetrievingRiddleException();
        } catch (IllegalArgumentException e) {
            throw new FailRetrievingRiddleException();
        }
        return result;
    }

    /**
     * Melakukan filter terhadap halaman kbbi yang telah didownload
     * untuk kemudian diambil sebuah kata.
     * @param meriamWebsterHtml halaman Merriam Webster
     * @return kata yang dijadikan tebak-tebakan
     * @throws IllegalArgumentException jika terjadi kesalahan pada saat mengambil data
     */
    String getRandomWord(String meriamWebsterHtml) throws IllegalArgumentException {
        List<String> allWord = new ArrayList<>();
        Matcher m = Pattern.compile("/thesaurus/(.*?)\"").matcher(meriamWebsterHtml);
        while (m.find()) {
            String wordUrl = m.group();
            String word = wordUrl.substring(11, wordUrl.length() - 1);
            allWord.add(word);
        }
        Random random = new Random();
        int countWord = allWord.size();
        int indexWord = random.nextInt(countWord);
        String result = allWord.get(indexWord);
        result = result.split(" ")[0];
        return result;
    }

    /**
     * Melakukan filter terhadap halaman Merriam Webster yang telah didownload
     * untuk kemudian diambil sebuah kata.
     * @param meriamWebsterPage halaman Meriam Webster
     * @return kata yang dijadikan tebak-tebakan
     * @throws IllegalArgumentException jika terjadi kesalahan pada saat mengambil data
     */
    String getMeriamWebsterHtml(String meriamWebsterPage) throws IOException {
        URL url = null;
        String result = "";
        url = new URL(meriamWebsterPage);
        BufferedReader readr =
                new BufferedReader(new InputStreamReader(url.openStream()));
        String line;
        while ((line = readr.readLine()) != null) {
            result += line;
        }
        return result;
    }

    /**
     * Membuat halaman Meriam Webster dan memilih angka random halaman.
     * @return url halaman Meriam Webster
     */
    String randomMeriamWebsterPage() {
        Random random = new Random();
        int numberOfPage = random.nextInt(26) + 97;
        String thePage = (char)numberOfPage + "";
        return "https://www.merriam-webster.com/browse/thesaurus/" + thePage;
    }
}
