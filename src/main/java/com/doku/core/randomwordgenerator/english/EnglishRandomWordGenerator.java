package com.doku.core.randomwordgenerator.english;

import com.doku.core.riddlegenerator.FailRetrievingRiddleException;

public interface EnglishRandomWordGenerator {

    /**
     * Memilih kata untuk tebak-tebakan secara random.
     * @return kata tebak-tebakan
     * @throws FailRetrievingRiddleException jika terjadi kesalahan pada saat mengambil json data
     */
    String generate() throws FailRetrievingRiddleException;
}
