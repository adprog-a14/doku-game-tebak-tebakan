package com.doku.core.randomwordgenerator.indonesia.website;

import com.doku.core.randomwordgenerator.indonesia.IndonesiaRandomWordGenerator;
import com.doku.core.riddlegenerator.FailRetrievingRiddleException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class KbbiCoId implements IndonesiaRandomWordGenerator {

    /**
     * Membuat page kbbi sesuai dengan nomor halamannya.
     * @param numberOfPage halaman
     * @return url kbbi
     */
    String createRandomKbbiRandomPage(int numberOfPage) {
        String result = "https://kbbi.co.id/daftar-kata";
        if (numberOfPage > 1) {
            result = "https://kbbi.co.id/daftar-kata?page=" + numberOfPage;
        }
        return result;
    }

    /**
     * Membuat halaman kbbi dan memilih angka random halaman.
     * @return url halaman kbbi
     */
    String randomKbbiPage() {
        Random random = new Random();
        int numberOfPage = random.nextInt(106) + 1;
        return createRandomKbbiRandomPage(numberOfPage);
    }

    /**
     * Melakukan filter terhadap halaman kbbi yang telah didownload
     * untuk kemudian diambil sebuah kata.
     * @param kbbiHtml halaman kbbi
     * @return kata yang dijadikan tebak-tebakan
     * @throws IllegalArgumentException jika terjadi kesalahan pada saat mengambil data
     */
    String getRandomWord(String kbbiHtml) throws IllegalArgumentException {
        List<String> allWord = new ArrayList<>();
        Matcher m = Pattern.compile("/arti-kata/(.*?)\"").matcher(kbbiHtml);
        while (m.find()) {
            String wordUrl = m.group();
            String word = wordUrl.substring(11, wordUrl.length() - 1);
            allWord.add(word);
        }
        Random random = new Random();
        int countWord = allWord.size();
        int indexWord = random.nextInt(countWord);
        String result = allWord.get(indexWord);
        return result;
    }

    /**
     * Mendownload sebuah halaman kbbi sesuai dengan url-nya.
     * @param kbbiUrl url halaman kbbi
     * @return html halaman kbbi
     * @throws IOException jika terjadi kesalahan pada saat mengambil data
     */
    String getKbbiHtml(String kbbiUrl) throws IOException {
        String result = "";
        URL url = new URL(kbbiUrl);
        BufferedReader readr =
                new BufferedReader(new InputStreamReader(url.openStream()));
        String line;
        while ((line = readr.readLine()) != null) {
            result += line;
        }
        return result;
    }

    /**
     * Memilih kata untuk tebak-tebakan secara random.
     * @return kata tebak-tebakan
     * @throws FailRetrievingRiddleException jika terjadi kesalahan pada saat mengambil data
     */
    @Override
    public String generate() throws FailRetrievingRiddleException {
        String result = null;
        try {
            String kbbiUrl = randomKbbiPage();
            String kbbiHtml = getKbbiHtml(kbbiUrl);
            result = getRandomWord(kbbiHtml);
        } catch (IOException e) {
            throw new FailRetrievingRiddleException();
        } catch (IllegalArgumentException e) {
            throw new FailRetrievingRiddleException();
        }
        return result;
    }
}
