package com.doku.core.randomwordgenerator.indonesia.database;

import com.doku.core.randomwordgenerator.indonesia.IndonesiaRandomWordGenerator;

/**
 * Hanya untuk ilustrasi maintenance.
 */
public class IndonesiaDatabase implements IndonesiaRandomWordGenerator {
    @Override
    public String generate() {
        return null;
    }
}
