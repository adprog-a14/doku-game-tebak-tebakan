package com.doku.core.randomwordgenerator.indonesia;

import com.doku.core.riddlegenerator.FailRetrievingRiddleException;

public interface IndonesiaRandomWordGenerator {

    /**
     * Memilih kata untuk tebak-tebakan secara random.
     * @return kata tebak-tebakan
     * @throws FailRetrievingRiddleException jika terjadi kesalahan pada saat mengambil data
     */
    String generate() throws FailRetrievingRiddleException;
}
