package com.doku.core.dictionary.english.api;

import com.doku.core.dictionary.english.EnglishDictionary;
import com.doku.core.riddlegenerator.FailRetrievingRiddleException;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public abstract class EnglishDictionaryApi implements EnglishDictionary {

    /**
     * Membuat url API.
     * @param word kata tebak-tebakan
     * @return
     */
    abstract String createWordUrl(String word);

    /**
     * Membuat json data yang akan dijadikan tebak-tebakan.
     * @param jsonRaw json data yang masih raw
     * @return json data tebak-tebakan yang sudah difilter
     */
    abstract ObjectNode createObjectNode(ObjectNode jsonRaw);

    /**
     * Mengkonversi dari string ke json.
     * @param stringResult string
     * @return Objectnode yang seperti json
     * @throws IOException jika terjadi kesalahan pada saat mengambil json data
     */
    ObjectNode convertToJson(String stringResult) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonFactory factory = mapper.getFactory();
        JsonParser jsonParser = factory.createParser(stringResult);
        ObjectNode jsonData = mapper.readTree(jsonParser);
        return jsonData;
    }

    /**
     * Mendapatkan hasil json setelah menembak API.
     * @param wordUrl url API
     * @return json data
     * @throws IOException jika terjadi kesalahan pada saat mengambil json data
     */
    ObjectNode getWordUrlResult(String wordUrl) throws IOException {
        String stringResult = "";
        URL url = new URL(wordUrl);
        BufferedReader readr =
                new BufferedReader(new InputStreamReader(url.openStream()));
        String line;
        while ((line = readr.readLine()) != null) {
            stringResult += line;
        }
        stringResult = cleanJsonArrayNode(stringResult);
        ObjectNode jsonResult = convertToJson(stringResult);
        return jsonResult;
    }

    /**
     * Membersihkan noise dari string json yang telah diambil.
     * @param stringResult String json
     * @return String yang sudah bersih
     */
    String cleanJsonArrayNode(String stringResult) {
        if (stringResult.charAt(0) == '[') {
            return stringResult.substring(1, stringResult.length() - 1);
        } else {
            return stringResult;
        }
    }

    /**
     * Mengecek kata dari dictionary untuk mendapatkan properti kata tersebut
     * seperti definisi dan jenis kata.
     * @param word kata tebak-tebakan yang akan dicek
     * @return json data tebak-tebakan
     * @throws FailRetrievingRiddleException jika terjadi kesalahan pada saat mengambil json data
     */
    @Override
    public ObjectNode check(String word) throws FailRetrievingRiddleException {
        String wordUrl = this.createWordUrl(word);
        ObjectNode wholeJsonData = null;
        try {
            wholeJsonData = getWordUrlResult(wordUrl);
        } catch (IOException e) {
            throw new FailRetrievingRiddleException();
        }

        ObjectNode result = this.createObjectNode(wholeJsonData);
        return result;
    }
}
