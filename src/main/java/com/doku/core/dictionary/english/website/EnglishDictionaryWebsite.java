package com.doku.core.dictionary.english.website;

import com.doku.core.dictionary.english.EnglishDictionary;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Hanya untuk ilustrasi maintenance.
 */
public abstract class EnglishDictionaryWebsite implements EnglishDictionary {
    @Override
    public ObjectNode check(String word) {
        return null;
    }
}
