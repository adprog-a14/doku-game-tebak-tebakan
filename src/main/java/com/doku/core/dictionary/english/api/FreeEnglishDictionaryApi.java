package com.doku.core.dictionary.english.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.HashMap;
import java.util.Map;

public class FreeEnglishDictionaryApi extends EnglishDictionaryApi {
    private final Map<String, String> outputAttributes;

    /**
     * Merupakan konstruktor yang menjadi regex pencari kata dan definisinya.
     */
    public FreeEnglishDictionaryApi() {
        outputAttributes = new HashMap<>();
        outputAttributes.put("name", "/word");
        outputAttributes.put("encryptedAnswer", "/word");
        outputAttributes.put("partOfSpeech", "/meanings/0/partOfSpeech");
        outputAttributes.put("definition", "/meanings/0/definitions/0/definition");
    }

    /**
     * Membuat url API.
     * @param word kata tebak-tebakan
     * @return
     */
    @Override
    String createWordUrl(String word) {
        return "https://api.dictionaryapi.dev/api/v2/entries/en_US/" + word;
    }

    /**
     * Membuat json data yang akan dijadikan tebak-tebakan.
     * @param jsonRaw json data yang masih raw
     * @return json data tebak-tebakan yang sudah difilter
     */
    @Override
    ObjectNode createObjectNode(ObjectNode jsonRaw) {
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        outputAttributes.forEach((attrName, attrPointer) -> {
            JsonNode attrVal = jsonRaw.at(attrPointer);
            String attrValClean = attrVal.toString().replaceAll("^[\"]+|[\"]+$", "");
            objectNode.put(attrName, attrValClean);
        });
        return objectNode;
    }
}
