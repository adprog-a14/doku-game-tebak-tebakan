package com.doku.core.dictionary.english.database;

import com.doku.core.dictionary.english.EnglishDictionary;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Hanya untuk ilustrasi maintenance.
 */
public class EnglishDatabase implements EnglishDictionary {

    @Override
    public ObjectNode check(String word) {
        return null;
    }
}
