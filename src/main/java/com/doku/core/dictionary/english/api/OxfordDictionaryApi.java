package com.doku.core.dictionary.english.api;

import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Hanya untuk ilustrasi maintenance.
 */
public class OxfordDictionaryApi extends EnglishDictionaryApi {

    @Override
    String createWordUrl(String word) {
        return null;
    }

    @Override
    ObjectNode createObjectNode(ObjectNode jsonRaw) {
        return null;
    }
}
