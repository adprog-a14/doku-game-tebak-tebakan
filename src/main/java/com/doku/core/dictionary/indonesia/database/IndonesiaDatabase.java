package com.doku.core.dictionary.indonesia.database;

import com.doku.core.dictionary.indonesia.IndonesiaDictionary;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Hanya untuk ilustrasi maintenance.
 */
public class IndonesiaDatabase implements IndonesiaDictionary {
    @Override
    public ObjectNode check(String word) {
        return null;
    }
}
