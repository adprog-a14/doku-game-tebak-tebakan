package com.doku.core.dictionary.indonesia.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.HashMap;
import java.util.Map;

public class KategloApi extends IndonesiaDictionaryApi {
    private final Map<String, String> outputAttributes;

    /**
     * Untuk membuat regex pencari kata dan definisinya.
     */
    public KategloApi() {
        outputAttributes = new HashMap<>();
        outputAttributes.put("name", "/kateglo/phrase");
        outputAttributes.put("encryptedAnswer", "/kateglo/phrase");
        outputAttributes.put("partOfSpeech", "/kateglo/lex_class_ref");
        outputAttributes.put("definition", "/kateglo/definition/0/def_text");
    }

    /**
     * Membuat url API.
     * @param word kata tebak-tebakan
     * @return
     */
    @Override
    String createWordUrl(String word) {
        return "http://kateglo.com/api.php?format=json&phrase=" + word;
    }

    /**
     * Membuat json data yang akan dijadikan tebak-tebakan.
     * @param jsonRaw json data yang masih raw
     * @return json data tebak-tebakan yang sudah difilter
     */
    @Override
    ObjectNode createObjectNode(ObjectNode jsonRaw) {
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        outputAttributes.forEach((attrName, attrPointer) -> {
            JsonNode attrVal = jsonRaw.at(attrPointer);
            String attrValClean = attrVal.toString().replaceAll("^[\"]+|[\"]+$", "");
            objectNode.put(attrName, attrValClean);
        });
        return objectNode;
    }
}
