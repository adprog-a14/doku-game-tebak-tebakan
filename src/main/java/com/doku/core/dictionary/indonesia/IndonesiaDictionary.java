package com.doku.core.dictionary.indonesia;

import com.doku.core.riddlegenerator.FailRetrievingRiddleException;
import com.fasterxml.jackson.databind.node.ObjectNode;

public interface IndonesiaDictionary {

    /**
     * Mengecek kata dari dictionary untuk mendapatkan properti kata tersebut
     * seperti definisi dan jenis kata.
     * @param word kata tebak-tebakan yang akan dicek
     * @return json data tebak-tebakan
     * @throws FailRetrievingRiddleException jika terjadi kesalahan pada saat mengambil json data
     */
    ObjectNode check(String word) throws FailRetrievingRiddleException;
}
