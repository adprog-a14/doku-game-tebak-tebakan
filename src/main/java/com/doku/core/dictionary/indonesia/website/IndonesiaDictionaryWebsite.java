package com.doku.core.dictionary.indonesia.website;

import com.doku.core.dictionary.indonesia.IndonesiaDictionary;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Hanya untuk ilustarasi maintenance.
 */
public abstract class IndonesiaDictionaryWebsite implements IndonesiaDictionary {

    @Override
    public ObjectNode check(String word) {
        return null;
    }
}
