package com.doku.core.dictionary.indonesia.api;

import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Hanya untuk ilustrasi maintenance.
 */
public class KbbiApi extends IndonesiaDictionaryApi {
    @Override
    String createWordUrl(String word) {
        return null;
    }

    @Override
    ObjectNode createObjectNode(ObjectNode jsonRaw) {
        return null;
    }
}
