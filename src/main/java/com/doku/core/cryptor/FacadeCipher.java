package com.doku.core.cryptor;

import java.util.ArrayList;
import java.util.List;

public class FacadeCipher extends RiddleCryptor {
    private final List<RiddleCryptor> riddleCryptorList;

    /**
     * Setup facade agar user bisa langsung memakainya.
     */
    public FacadeCipher() {
        riddleCryptorList = new ArrayList<>();
        riddleCryptorList.add(new CaesarCipher());
        riddleCryptorList.add(new ViginereCipher());
    }

    /**
     * Menambah kriptor.
     * @param riddleCryptor kriptor yang akan ditambah
     */
    public void addCryptor(RiddleCryptor riddleCryptor) {
        riddleCryptorList.add(riddleCryptor);
    }

    /**
     * Meremove kriptor.
     * @param riddleCryptor kriptor yang akan diremove
     */
    public void removeCryptor(RiddleCryptor riddleCryptor) {
        riddleCryptorList.remove(riddleCryptor);
    }

    /**
     * Melakukan enkripsi sesuai urutan masuk kriptor di riddleCryptorList.
     * @param word kata yang akan dienkripsi
     * @return kata yang sudah dienkripsi
     */
    @Override
    public String encrypt(String word) {
        String result = word;
        for (RiddleCryptor riddleCryptor : riddleCryptorList) {
            result = riddleCryptor.encrypt(result);
        }
        return result;
    }

    /**
     * Melakukan dekripsi dengan urutan terbalik dari urutan masuk riddleCryptorList.
     * @param word kata yang akan didekripsi
     * @return kata yang sudah didekripsi
     */
    @Override
    public String decrypt(String word) {
        String result = word;
        for (int i = riddleCryptorList.size() - 1; i > -1; i--) {
            RiddleCryptor riddleCryptor = riddleCryptorList.get(i);
            result = riddleCryptor.decrypt(result);
        }
        return result;
    }
}
