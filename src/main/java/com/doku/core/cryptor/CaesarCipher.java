package com.doku.core.cryptor;

public class CaesarCipher extends RiddleCryptor {
    private final int shift;

    /**
     * Konstruktor dengan geser defaultnya.
     */
    public CaesarCipher() {
        this(5);
    }

    /**
     * Konstruktor dengan kunci input.
     * @param change pergesaran caesar cipher
     */
    public CaesarCipher(int change) {
        this.shift = change;
    }

    /**
     * Melakukan enkripsi.
     * @param word kata yang dienkripsi
     * @return kata yang sudah dienkripsi
     */
    @Override
    public String encrypt(String word) {
        StringBuffer result = new StringBuffer();
        for (int i = 0; i < word.length(); i++) {
            if (Character.isUpperCase(word.charAt(i))) {
                char ch = (char)(((int)word.charAt(i) + shift - 65) % 26 + 65);
                result.append(ch);
            } else {
                char ch = (char)(((int)word.charAt(i)
                        + shift - 97) % 26 + 97);
                result.append(ch);
            }
        }
        return result.toString();
    }

    /**
     * Melakukan dekripsi.
     * @param word kata yang didekripsi
     * @return kata yang sudah didekripsi
     */
    @Override
    public String decrypt(String word) {
        StringBuffer result = new StringBuffer();
        for (int i = 0; i < word.length(); i++) {
            if (Character.isUpperCase(word.charAt(i))) {
                char ch = (char)((((((int)word.charAt(i) - shift - 65) % 26) + 26) % 26) + 65);
                result.append(ch);
            } else {
                char ch = (char)(((((int)word.charAt(i)
                        - shift - 97) % 26) + 26) % 26 + 97);
                result.append(ch);
            }
        }
        return result.toString();
    }
}
