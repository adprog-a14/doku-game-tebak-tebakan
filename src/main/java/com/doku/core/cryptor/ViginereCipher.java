package com.doku.core.cryptor;

public class ViginereCipher extends RiddleCryptor {
    private final String keyword;

    /**
     * Konstruktor dengan kunci defaultnya.
     */
    public ViginereCipher() {
        this("bismillah");
    }

    /**
     * Konstruktor dengan kunci input.
     * @param keyword kunci viginere cipher
     */
    public ViginereCipher(String keyword) {
        this.keyword = keyword;
    }

    /**
     * Melakukan enkripsi.
     * @param word kata yang dienkripsi
     * @return kata yang sudah dienkripsi
     */
    @Override
    public String encrypt(String word) {
        String key = generateKey(word, keyword);
        StringBuffer result = new StringBuffer();
        for (int i = 0; i < word.length(); i++) {
            if (Character.isUpperCase(word.charAt(i))) {
                char ch = (char)(((int)word.charAt(i) + key.charAt(i) - 65) % 26 + 65);
                result.append(ch);
            } else {
                char ch = (char)(((int)word.charAt(i)
                        + key.charAt(i) - 97) % 26 + 97);
                result.append(ch);
            }
        }
        return result.toString();
    }

    /**
     * Melakukan dekripsi.
     * @param word kata yang didekripsi
     * @return kata yang sudah didekripsi
     */
    @Override
    public String decrypt(String word) {
        String key = generateKey(word, keyword);
        StringBuffer result = new StringBuffer();
        for (int i = 0; i < word.length(); i++) {
            if (Character.isUpperCase(word.charAt(i))) {
                char ch = (char)((((((int)word.charAt(i) - key.charAt(i)
                        - 65) % 26) + 26) % 26) + 65);
                result.append(ch);
            } else {
                char ch = (char)(((((int)word.charAt(i)
                        - key.charAt(i) - 97) % 26) + 26) % 26 + 97);
                result.append(ch);
            }
        }
        return result.toString();
    }

    /**
     * Menyesuaikan kunci dengan kata yang akan dienkripsi/dekripsi.
     * @param word kata yang akan dienkripsi/dekripsi
     * @param key kunci yang akan disesuikan
     * @return kunci yang telah disesuaikan
     */
    private String generateKey(String word, String key) {
        if (key.length() > word.length()) {
            return key.substring(0, word.length());
        }
        int x = key.length();
        for (int i = 0; key.length() < word.length(); i++) {
            key += (key.charAt(i % x));
        }
        return key;
    }
}
