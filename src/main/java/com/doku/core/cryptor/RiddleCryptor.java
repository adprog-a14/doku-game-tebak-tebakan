package com.doku.core.cryptor;

import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.Random;

public abstract class RiddleCryptor {

    /**
     * Untuk melakukan enkripsi.
     * @param data json yang akan dienkripsi
     */
    public void doEncryption(ObjectNode data) {
        String answer = data.get("encryptedAnswer").toString();
        answer = answer.replaceAll("^[\"]+|[\"]+$", "");
        data.put("encryptedAnswer", this.encrypt(answer));
        data.put("name", shuffleLetter(answer));
    }

    /**
     * Untuk melakukan dekripsi.
     * @param inputString input yang dienkripsi
     * @return input setelah didekripsi
     */
    private String shuffleLetter(String inputString) {
        Random random = new Random();
        char[] a = inputString.toCharArray();
        for (int i = 0; i < a.length; i++) {
            int j = random.nextInt(a.length);
            char temp = a[i];
            a[i] = a[j];
            a[j] = temp;
        }
        return new String(a);
    }

    /**
     * Melakukan dekripsi.
     * @param word kata yang akan didekripsi
     * @return kata yang sudah didekripsi
     */
    public String doDecryption(String word) {
        return decrypt(word);
    }

    /**
     * Abstract method enkripsi untuk child class.
     * @param word kata yang dienkripsi
     * @return kata yang sudah dienkripsi
     */
    public abstract String encrypt(String word);

    /**
     * Abstract method dekripsi untuk child class.
     * @param word kata yang didekripsi
     * @return kata yang sudah didekripsi
     */
    public abstract String decrypt(String word);
}
