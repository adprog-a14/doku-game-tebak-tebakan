package com.doku.core.riddlegenerator;

import com.doku.core.dictionary.indonesia.IndonesiaDictionary;
import com.doku.core.dictionary.indonesia.api.KategloApi;
import com.doku.core.randomwordgenerator.indonesia.IndonesiaRandomWordGenerator;
import com.doku.core.randomwordgenerator.indonesia.website.KbbiCoId;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class IndonesiaRiddleGenerator extends RiddleGenerator {
    private IndonesiaRandomWordGenerator indonesiaRandomWordGenerator;
    private IndonesiaDictionary indonesiaDictionary;

    /**
     * Konstruktor dengan atribut defaultnya.
     */
    public IndonesiaRiddleGenerator() {
        this.indonesiaRandomWordGenerator = new KbbiCoId();
        this.indonesiaDictionary = new KategloApi();
    }

    /**
     * Setter random word generator.
     * @param indonesiaRandomWordGenerator kelas interface untuk melakukan generator kata
     */
    public void setRandomWordGenerator(IndonesiaRandomWordGenerator indonesiaRandomWordGenerator) {
        this.indonesiaRandomWordGenerator = indonesiaRandomWordGenerator;
    }

    /**
     * Setter dictionary.
     * @param indonesiaDictionary kelas interface untuk melakukan pengecekan kata
     */
    public void setDictionary(IndonesiaDictionary indonesiaDictionary) {
        this.indonesiaDictionary = indonesiaDictionary;
    }

    /**
     * Mengecek kata tersebut pada sebuah dictionary untuk mengembalikan properti dari kata
     * tersebut seperti definisi dan jenis kata.
     * @param word kata yang dicek
     * @return json data tebak-tebakan
     * @throws FailRetrievingRiddleException jika terjadi kesalahan pada saat mengambil data
     */
    @Override
    protected ObjectNode checkDictionary(String word) throws FailRetrievingRiddleException {
        return indonesiaDictionary.check(word);
    }

    /**
     * Membuat kata-kata random sesuai dengan cara child class tersebut.
     * @return kata random untuk tebak-tebakan
     * @throws FailRetrievingRiddleException jika terjadi kesalahan pada saat mengambil data
     */
    @Override
    protected String produceRandomWord() throws FailRetrievingRiddleException {
        return indonesiaRandomWordGenerator.generate();
    }
}
