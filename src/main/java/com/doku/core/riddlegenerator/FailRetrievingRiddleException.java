package com.doku.core.riddlegenerator;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class FailRetrievingRiddleException extends Exception {

    /**
     * Untuk membuat json data yang menunjukan bahwa error terjadi.
     * @return json data
     */
    public ObjectNode getObjectNode() {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode objectNode = mapper.createObjectNode();
        objectNode.put("name", "404error");
        objectNode.put("encryptedAnswer", "404error");
        return objectNode;
    }
}
