package com.doku.core.riddlegenerator;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public abstract class RiddleGenerator {

    /**
     * Template method untuk membuat suatu output.
     * @return Json object
     */
    public ObjectNode generate() {
        ObjectNode result = null;
        try {
            String word = this.produceRandomWord();
            result = this.checkDictionary(word);
        } catch (FailRetrievingRiddleException e) {
            result = e.getObjectNode();
        }
        return result;
    }

    /**
     * Membuat kata-kata random sesuai dengan cara child class tersebut.
     * @return kata random untuk tebak-tebakan
     * @throws FailRetrievingRiddleException jika terjadi kesalahan pada saat mengambil data
     */
    protected abstract String produceRandomWord() throws FailRetrievingRiddleException;

    /**
     * Mengecek kata tersebut pada sebuah dictionary untuk mengembalikan properti dari kata
     * tersebut seperti definisi dan jenis kata.
     * @param word kata yang dicek
     * @return json data tebak-tebakan
     * @throws FailRetrievingRiddleException jika terjadi kesalahan pada saat mengambil data
     */
    protected abstract ObjectNode checkDictionary(String word) throws FailRetrievingRiddleException;
}
