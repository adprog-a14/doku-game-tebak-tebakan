package com.doku.core.riddlegenerator;

import com.doku.core.dictionary.english.EnglishDictionary;
import com.doku.core.dictionary.english.api.FreeEnglishDictionaryApi;
import com.doku.core.randomwordgenerator.english.EnglishRandomWordGenerator;
import com.doku.core.randomwordgenerator.english.website.MerriamWebsterCom;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class EnglishRiddleGenerator extends RiddleGenerator {
    private EnglishRandomWordGenerator englishRandomWordGenerator;
    private EnglishDictionary englishDictionary;

    /**
     * Konstruktor dengan atribut defaultnya.
     */
    public EnglishRiddleGenerator() {
        this.englishRandomWordGenerator = new MerriamWebsterCom();
        this.englishDictionary = new FreeEnglishDictionaryApi();
    }

    /**
     * Membuat kata-kata random sesuai dengan cara child class tersebut.
     * @return kata random untuk tebak-tebakan
     * @throws FailRetrievingRiddleException jika terjadi kesalahan pada saat mengambil data
     */
    @Override
    protected String produceRandomWord() throws FailRetrievingRiddleException {
        return englishRandomWordGenerator.generate();
    }

    /**
     * Mengecek kata tersebut pada sebuah dictionary untuk mengembalikan properti dari kata
     * tersebut seperti definisi dan jenis kata.
     * @param word kata yang dicek
     * @return json data tebak-tebakan
     * @throws FailRetrievingRiddleException jika terjadi kesalahan pada saat mengambil data
     */
    @Override
    protected ObjectNode checkDictionary(String word) throws FailRetrievingRiddleException {
        return englishDictionary.check(word);
    }

    /**
     * Setter random word generator.
     * @param englishRandomWordGenerator kelas interface untuk melakukan generator kata
     */
    public void setRandomWordGenerator(EnglishRandomWordGenerator englishRandomWordGenerator) {
        this.englishRandomWordGenerator = englishRandomWordGenerator;
    }

    /**
     * Setter dictionary.
     * @param englishDictionary kelas interface untuk melakukan pengecekan kata
     */
    public void setDictionary(EnglishDictionary englishDictionary) {
        this.englishDictionary = englishDictionary;
    }
}
