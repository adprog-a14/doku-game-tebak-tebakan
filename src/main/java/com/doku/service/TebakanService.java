package com.doku.service;

import com.fasterxml.jackson.databind.node.ObjectNode;

public interface TebakanService {

    /**
     * Membuat tebak-tebakan.
     * @param language bahasa untuk tebak-tebakan.
     * @return json data tebak-tebakan
     */
    ObjectNode generateRiddle(String language);

    /**
     * Mengecek jawaban user.
     * @param encryptedAnswer jawaban tebak-tebakan yang telah dienkripsi
     * @param answer jawaban user
     * @return true atau false tergantung benar salah jawaban user
     */
    Boolean checkAnswer(String encryptedAnswer, String answer);
}
