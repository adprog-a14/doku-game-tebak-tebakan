package com.doku.service;

import com.doku.core.cryptor.CaesarCipher;
import com.doku.core.cryptor.FacadeCipher;
import com.doku.core.cryptor.RiddleCryptor;
import com.doku.core.cryptor.ViginereCipher;
import com.doku.core.riddlegenerator.EnglishRiddleGenerator;
import com.doku.core.riddlegenerator.IndonesiaRiddleGenerator;
import com.doku.core.riddlegenerator.RiddleGenerator;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Service;

@Service
public class TebakanServiceImpl implements TebakanService {
    private final IndonesiaRiddleGenerator indonesiaRiddleGenerator;
    private final EnglishRiddleGenerator englishRiddleGenerator;
    private final RiddleCryptor riddleCryptor;

    /**
     * Konstruktor untuk setup segalanya.
     */
    public TebakanServiceImpl() {
        riddleCryptor = new FacadeCipher();
        indonesiaRiddleGenerator = new IndonesiaRiddleGenerator();
        englishRiddleGenerator = new EnglishRiddleGenerator();
    }

    /**
     * Membuat tebak-tebakan.
     * @param language bahasa untuk tebak-tebakan tersebut
     * @return json data tebak-tebakan
     */
    @Override
    public ObjectNode generateRiddle(String language) {
        RiddleGenerator riddleGenerator = checkLanguage(language);
        ObjectNode riddle = riddleGenerator.generate();
        riddleCryptor.doEncryption(riddle);
        return riddle;
    }

    /**
     * Mengecek jawaban user.
     * @param encryptedAnswer jawaban asli yang telah dienkripsi
     * @param userAnswer jawaban user
     * @return true atau false tergantung benar salah jawaban user
     */
    @Override
    public Boolean checkAnswer(String encryptedAnswer, String userAnswer) {
        String realAnswer = riddleCryptor.doDecryption(encryptedAnswer);
        return realAnswer.equals(userAnswer);
    }

    /**
     * Mengecek bahasa yang digunakan untuk riddleGenerator.
     * @param language bahasa yang dicek
     * @return kelas riddleGenerator sesuai bahasa
     */
    private RiddleGenerator checkLanguage(String language) {
        RiddleGenerator riddleGenerator = null;
        if (language.equals("English")) {
            riddleGenerator = englishRiddleGenerator;
        } else {
            riddleGenerator = indonesiaRiddleGenerator;
        }
        return riddleGenerator;
    }
}
