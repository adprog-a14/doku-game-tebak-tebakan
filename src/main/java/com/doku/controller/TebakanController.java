package com.doku.controller;

import com.doku.service.TebakanService;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TebakanController {

    @Autowired
    TebakanService tebakanService;

    /**
     * Url untuk membuat tebak-tebakan.
     * @param language bahasa untuk tebak-tebakan. Hanya tersedia Inggris dan Indonesia
     * @return json data
     */
    @GetMapping(value = "/game-tebakan", produces = "application/json")
    public ResponseEntity startGame(
            @RequestParam(value = "language") String language
    ) {
        ObjectNode objectNode = tebakanService.generateRiddle(language);
        return ResponseEntity.ok(objectNode);
    }

    /**
     * Mengecek jawaban user.
     * @param encryptedAnswer jawaban asli yang telah dienkripsi
     * @param answer jawaban user
     * @return true atau false tergantung apakah sesuai dengan jawaban asli
     */
    @PostMapping(value = "/game-tebakan/cek")
    public ResponseEntity checkAnswer(
            @RequestParam("encryptedAnswer") String encryptedAnswer,
            @RequestParam("answer") String answer
    ) {
        Boolean result = tebakanService.checkAnswer(encryptedAnswer, answer);
        return ResponseEntity.ok(result);
    }
}
