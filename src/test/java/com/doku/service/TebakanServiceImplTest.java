package com.doku.service;

import static org.junit.jupiter.api.Assertions.assertTrue;

import com.doku.core.cryptor.RiddleCryptor;
import com.doku.core.riddlegenerator.EnglishRiddleGenerator;
import com.doku.core.riddlegenerator.IndonesiaRiddleGenerator;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;



@ExtendWith(MockitoExtension.class)
class TebakanServiceImplTest {

    @Mock
    private IndonesiaRiddleGenerator indonesiaRiddleGenerator;

    @Mock
    private EnglishRiddleGenerator englishRiddleGenerator;

    @Mock
    private RiddleCryptor riddleCryptor;

    @InjectMocks
    private TebakanServiceImpl tebakanService;


    @BeforeEach
    void setUp() {
        tebakanService = new TebakanServiceImpl();
    }

    @Test
    void testCheckAnswerMethod() {
        String userAnswer = "mahfuz";
        String encryptedAnswer = "lgxpai";

        Boolean realAnswer = tebakanService.checkAnswer(encryptedAnswer, userAnswer);
        assertTrue(realAnswer);
    }

    @Test
    void testGenerateRiddleMethodWithInputEnglish() {
        String language = "English";
        ObjectNode objectNode = tebakanService.generateRiddle(language);
        assertTrue(objectNode.getClass().getName()
                .equals("com.fasterxml.jackson.databind.node.ObjectNode")
                || objectNode == null);
    }

    @Test
    void testGenerateRiddleMethodWithInputIndonesia() {
        String language = "Indonesia";
        ObjectNode objectNode = tebakanService.generateRiddle(language);
        assertTrue(objectNode.getClass().getName()
                .equals("com.fasterxml.jackson.databind.node.ObjectNode")
                || objectNode == null);
    }
}