package com.doku.core.dictionary.indonesia.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;



class KategloApiTest {

    private KategloApi kategloApi;

    @BeforeEach
    void setUp() {
        kategloApi = new KategloApi();
    }

    @Test
    void testCreateWordUrlMethod() {
        String result = kategloApi.createWordUrl("tes");
        assertEquals("http://kateglo.com/api.php?format=json&phrase=tes", result);
    }

    @Test
    void testCreateObjectNodeMethod() {
        String stringJson = "{\"kateglo\":{\"phrase\":\"mukmin\",\""
                + "lex_class_ref\":\"nomina\",\"definition"
                + "\":[{\"def_text\":\"Orang Islam\"}]}}";
        ObjectNode objectNode = null;
        try {
            objectNode = kategloApi.convertToJson(stringJson);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ObjectNode result = kategloApi.createObjectNode(objectNode);
        assertTrue(result.has("name"));
        assertTrue(result.has("encryptedAnswer"));
        assertTrue(result.has("partOfSpeech"));
        assertTrue(result.has("definition"));
    }
}