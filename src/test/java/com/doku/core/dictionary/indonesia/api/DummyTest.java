package com.doku.core.dictionary.indonesia.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.doku.core.dictionary.indonesia.database.IndonesiaDatabase;
import com.doku.core.dictionary.indonesia.website.KbbiCoId;
import com.doku.core.riddlegenerator.FailRetrievingRiddleException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.Test;


public class DummyTest {

    @Test
    void testDummyClass() throws Exception {

        IndonesiaDatabase indonesiaDatabase = new IndonesiaDatabase();
        KbbiCoId kbbiCoId = new KbbiCoId();
        KbbiApi kbbiApi = new KbbiApi();

        assertEquals(null, indonesiaDatabase.check("tes"));
        assertEquals(null, kbbiCoId.check("tes"));

        try {
            kbbiApi.check("tes");
        } catch (FailRetrievingRiddleException e) {
            assertTrue(true);
        }

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        assertEquals(null, kbbiApi.createObjectNode(objectNode));
    }
}
