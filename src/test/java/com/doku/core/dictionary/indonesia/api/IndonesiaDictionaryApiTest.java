package com.doku.core.dictionary.indonesia.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import com.doku.core.riddlegenerator.FailRetrievingRiddleException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import java.net.URL;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class IndonesiaDictionaryApiTest {

    private IndonesiaDictionaryApi indonesiaDictionaryApi;

    @BeforeEach
    void setUp() {
        indonesiaDictionaryApi = Mockito.mock(
                IndonesiaDictionaryApi.class, Mockito.CALLS_REAL_METHODS);
    }

    @Test
    void testIndonesiaDictionaryTemplateMethodSuccess() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        String mockUrl = "http://kateglo.com/api.php?format=json&phrase=tes";
        when(indonesiaDictionaryApi.createWordUrl("test")).thenReturn(mockUrl);
        when(indonesiaDictionaryApi.getWordUrlResult(mockUrl)).thenReturn(objectNode);
        when(indonesiaDictionaryApi.createObjectNode(objectNode)).thenReturn(objectNode);

        ObjectNode result = indonesiaDictionaryApi.check("test");
        assertEquals(objectNode, result);
    }

    @Test
    void testIndonesiaDictionaryTemplateMethodFailed() throws Exception {
        String mockUrl = "http://kateglo.com/api.php?format=json&phrase=tes";
        when(indonesiaDictionaryApi.createWordUrl("tes")).thenReturn(mockUrl);
        when(indonesiaDictionaryApi.getWordUrlResult(mockUrl)).thenThrow(IOException.class);

        try {
            ObjectNode result = indonesiaDictionaryApi.check("tes");
        } catch (FailRetrievingRiddleException e) {
            assertTrue(true);
        }
    }

    @Test
    void testGetWordUrlResultMethod() throws Exception {
        try {
            URL url = new URL("http://www.google.com");
            url.openStream();
            ObjectNode result = indonesiaDictionaryApi.getWordUrlResult(
                    "http://kateglo.com/api.php?format=json&phrase=tes");
            assertEquals(result.getClass().getName(),
                    "com.fasterxml.jackson.databind.node.ObjectNode");
        } catch (IOException e) {
            ObjectNode result = indonesiaDictionaryApi.getWordUrlResult(
                    "http://kateglo.com/api.php?format=json&phrase=tes");
            assertEquals(null, result);
        }
    }
}