package com.doku.core.dictionary.english.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FreeEnglishDictionaryApiTest {

    private FreeEnglishDictionaryApi freeEnglishDictionaryApi;

    @BeforeEach
    void setUp() {
        freeEnglishDictionaryApi = new FreeEnglishDictionaryApi();
    }

    @Test
    void testCreateWordUrlMethod() {
        String result = freeEnglishDictionaryApi.createWordUrl("test");
        assertEquals("https://api.dictionaryapi.dev/api/v2/entries/en_US/test", result);
    }

    @Test
    void testCreateObjectNodeMethod() {
        String stringJson = "{\"word\":\"YOLO\",\"phonetics\":[{\"text\":\"/ˈjoʊloʊ/\",\"audio\":"
                + "\"https://lex-audio.useremarkable.com/mp3/yolo_1_us_1.mp3\"}],\"meanings\":"
                + "[{\"partOfSpeech\":\"abbreviation\",\"definitions\":[{\"definition\":\"You only "
                + "live once (expressing the view that one should make the most "
                + "of the present moment without worrying about the future, and often"
                + " used as a rationale for impulsive or"
                + " reckless behavior)\"}]}]}\"";
        ObjectNode objectNode = null;
        try {
            objectNode = freeEnglishDictionaryApi.convertToJson(stringJson);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ObjectNode result = freeEnglishDictionaryApi.createObjectNode(objectNode);
        assertTrue(result.has("name"));
        assertTrue(result.has("encryptedAnswer"));
        assertTrue(result.has("partOfSpeech"));
        assertTrue(result.has("definition"));
    }
}