package com.doku.core.dictionary.english.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import com.doku.core.riddlegenerator.FailRetrievingRiddleException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;



class EnglishDictionaryApiTest {

    private EnglishDictionaryApi englishDictionaryApi;

    @BeforeEach
    void setUp() {
        englishDictionaryApi = Mockito.mock(EnglishDictionaryApi.class, Mockito.CALLS_REAL_METHODS);
    }

    @Test
    void testEnglishDictionaryTemplateMethodSuccess() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();

        String mockUrl = "https://api.dictionaryapi.dev/api/v2/entries/en_US/test";
        when(englishDictionaryApi.createWordUrl("test")).thenReturn(mockUrl);
        when(englishDictionaryApi.getWordUrlResult(mockUrl)).thenReturn(objectNode);
        when(englishDictionaryApi.createObjectNode(objectNode)).thenReturn(objectNode);

        ObjectNode result = englishDictionaryApi.check("test");
        assertEquals(objectNode, result);
    }

    @Test
    void testEnglishDictionaryTemplateMethodFailed() throws Exception {
        String mockUrl = "https://api.dictionaryapi.dev/api/v2/entries/en_US/test";
        when(englishDictionaryApi.createWordUrl("test")).thenReturn(mockUrl);
        when(englishDictionaryApi.getWordUrlResult(mockUrl)).thenThrow(IOException.class);

        try {
            ObjectNode result = englishDictionaryApi.check("test");
        } catch (FailRetrievingRiddleException e) {
            assertTrue(true);
        }

    }

    @Test
    void testCleanJsonArrayNodeMethod() {
        String string = "[test]";
        String result = englishDictionaryApi.cleanJsonArrayNode(string);
        assertEquals("test", result);

        string = "test";
        result = englishDictionaryApi.cleanJsonArrayNode(string);
        assertEquals("test", result);
    }
}