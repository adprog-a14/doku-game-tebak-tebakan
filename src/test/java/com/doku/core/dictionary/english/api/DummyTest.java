package com.doku.core.dictionary.english.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.doku.core.dictionary.english.database.EnglishDatabase;
import com.doku.core.dictionary.english.website.MeriamWebsterCom;
import com.doku.core.riddlegenerator.FailRetrievingRiddleException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.Test;


public class DummyTest {

    @Test
    void testDummyClass() throws Exception {

        EnglishDatabase englishDatabase = new EnglishDatabase();
        assertEquals(null, englishDatabase.check("test"));

        MeriamWebsterCom meriamWebsterCom = new MeriamWebsterCom();
        assertEquals(null, meriamWebsterCom.check("test"));

        OxfordDictionaryApi oxfordDictionaryApi = new OxfordDictionaryApi();
        try {
            assertEquals(null, oxfordDictionaryApi.check("test"));

        } catch (FailRetrievingRiddleException e) {
            assertTrue(true);
        }

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        assertEquals(null, oxfordDictionaryApi.createObjectNode(objectNode));
    }
}
