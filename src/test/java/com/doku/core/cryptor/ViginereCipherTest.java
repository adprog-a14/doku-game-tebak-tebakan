package com.doku.core.cryptor;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class ViginereCipherTest {

    private ViginereCipher viginereCipher;

    @BeforeEach
    void setUp() {
        viginereCipher = new ViginereCipher();
    }

    @Test
    void testEncryptionMethod() {
        String string = "aaaHalOsemuazzz";
        String result = viginereCipher.encrypt(string);
        assertEquals("ublMbpSlegvlead", result);

        viginereCipher = new ViginereCipher("bismillahbismillahbismillah");
        string = "aaaHalOsemuazzz";
        result = viginereCipher.encrypt(string);
        assertEquals("ublMbpSlegvlead", result);
    }

    @Test
    void testDecryptionMethod() {
        String string = "ublMbpSlegvleAd";
        String result = viginereCipher.decrypt(string);
        assertEquals("aaaHalOsemuazZz", result);

        viginereCipher = new ViginereCipher("p");
        string = "iiiPitWamucihhh";
        result = viginereCipher.decrypt(string);
        assertEquals("aaaHalOsemuazzz", result);
    }
}