package com.doku.core.cryptor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;


class RiddleCryptorTest {

    private RiddleCryptor riddleCryptor;

    @BeforeEach
    void setUp() {
        riddleCryptor = Mockito.mock(RiddleCryptor.class, Mockito.CALLS_REAL_METHODS);
    }

    @Test
    void testDoEncryptionMethod() {
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        String testWord = "test";
        objectNode.put("name", testWord);
        objectNode.put("encryptedAnswer", testWord);
        when(riddleCryptor.encrypt(testWord)).thenReturn(testWord);

        riddleCryptor.doEncryption(objectNode);
        String resultName = objectNode.get("name").toString();
        verify(riddleCryptor).encrypt(testWord);
        testWord = "\"" + testWord + "\"";
        String resultEncryptedAnswer = objectNode.get("encryptedAnswer").toString();
        assertNotEquals(resultName, testWord);
        assertEquals(resultEncryptedAnswer, testWord);
    }

    @Test
    void testDoDecryptionMethod() {
        String testWord = "test";
        when(riddleCryptor.decrypt(testWord)).thenReturn(testWord);
        assertEquals(testWord, riddleCryptor.doDecryption(testWord));
    }
}