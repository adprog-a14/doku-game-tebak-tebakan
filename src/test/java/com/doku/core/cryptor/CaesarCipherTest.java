package com.doku.core.cryptor;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class CaesarCipherTest {

    private CaesarCipher caesarCipher;

    @BeforeEach
    void setUp() {
        caesarCipher = new CaesarCipher();
    }

    @Test
    void testEncryptionMethod() {
        String str = "aaaHalOsemuazzz";
        String result = caesarCipher.encrypt(str);
        assertEquals("fffMfqTxjrzfeee", result);
    }

    @Test
    void testDecyrptionMethod() {
        String str = "fffMfqTxjrzfeee";
        String result = caesarCipher.decrypt(str);
        assertEquals("aaaHalOsemuazzz", result);
    }
}