package com.doku.core.cryptor;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class FacadeCipherTest {

    private FacadeCipher facadeCipher;

    @BeforeEach
    void setUp() {
        facadeCipher = new FacadeCipher();
    }

    @Test
    void testEncryptionMethod() {
        facadeCipher.addCryptor(new ViginereCipher());
        String result = facadeCipher.encrypt("aaaHalOsemuazzz");
        assertEquals("thbWhyBjjfbbogm", result);
        facadeCipher.removeCryptor(new ViginereCipher());
    }

    @Test
    void testDecryptionMethod() {
        facadeCipher.addCryptor(new CaesarCipher());
        String result = facadeCipher.decrypt("fffMfqTxjrzfeee");
        assertEquals("bukXucFuznokptq", result);
        facadeCipher.removeCryptor(new CaesarCipher());
    }
}