package com.doku.core.riddlegenerator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;



class RiddleGeneratorTest {

    private ObjectNode expectedError;
    private RiddleGenerator riddleGenerator;

    @BeforeEach
    void setUp() {
        ObjectMapper mapper = new ObjectMapper();
        expectedError = mapper.createObjectNode();
        expectedError.put("name", "404error");
        expectedError.put("encryptedAnswer", "404error");
        riddleGenerator = mock(
                RiddleGenerator.class, Mockito.CALLS_REAL_METHODS);
    }

    @Test
    void testRiddleGeneratorTemplateMethodNoError() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode objectNode = mapper.createObjectNode();
        when(riddleGenerator.produceRandomWord()).thenReturn("test");
        when(riddleGenerator.checkDictionary("test"))
                .thenReturn(objectNode);
        assertEquals(objectNode, riddleGenerator.generate());
    }

    @Test
    void testRandomWordGeneratorThrowException() throws Exception {
        when(riddleGenerator.produceRandomWord())
                .thenThrow(FailRetrievingRiddleException.class);

        ObjectNode actual = riddleGenerator.generate();
        assertEquals(expectedError, actual);
    }

    @Test
    void testDictionaryApiThrowException() throws Exception {
        when(riddleGenerator.produceRandomWord()).thenReturn("test");
        when(riddleGenerator.checkDictionary("test"))
                .thenThrow(FailRetrievingRiddleException.class);

        ObjectNode actual = riddleGenerator.generate();
        assertEquals(expectedError, actual);
    }

    @Test
    void testRiddleGeneratorTemplateMethodRandomWordError() throws Exception {
        when(riddleGenerator.produceRandomWord())
                .thenThrow(FailRetrievingRiddleException.class);
        assertEquals(expectedError, riddleGenerator.generate());
    }

    @Test
    void testRiddleGeneratorTemplateMethodApiError() throws Exception {
        when(riddleGenerator.produceRandomWord()).thenReturn("test");
        when(riddleGenerator.checkDictionary("test"))
                .thenThrow(FailRetrievingRiddleException.class);
        assertEquals(expectedError, riddleGenerator.generate());
    }
}