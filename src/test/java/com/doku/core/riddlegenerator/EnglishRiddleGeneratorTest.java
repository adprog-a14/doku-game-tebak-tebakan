package com.doku.core.riddlegenerator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.doku.core.dictionary.english.EnglishDictionary;
import com.doku.core.randomwordgenerator.english.EnglishRandomWordGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
class EnglishRiddleGeneratorTest {

    @Mock
    private EnglishRandomWordGenerator englishRandomWordGenerator;

    @Mock
    private EnglishDictionary englishDictionary;

    @InjectMocks
    private EnglishRiddleGenerator englishRiddleGenerator;

    @BeforeEach
    void setUp() {
        englishRiddleGenerator = new EnglishRiddleGenerator();
    }

    @Test
    void testGenerateRandomWordMethod() throws Exception {
        when(englishRandomWordGenerator.generate()).thenReturn("test");
        englishRiddleGenerator.setRandomWordGenerator(englishRandomWordGenerator);

        String result = englishRiddleGenerator.produceRandomWord();
        assertEquals("test", result);
        verify(englishRandomWordGenerator).generate();
    }

    @Test
    void testCheckDictionaryMethod() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        when(englishDictionary.check("test")).thenReturn(objectNode);
        englishRiddleGenerator.setDictionary(englishDictionary);

        ObjectNode result = englishRiddleGenerator.checkDictionary("test");
        assertEquals(objectNode, result);
        verify(englishDictionary).check("test");
    }
}