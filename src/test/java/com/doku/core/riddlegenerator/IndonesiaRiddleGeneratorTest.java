package com.doku.core.riddlegenerator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.doku.core.dictionary.indonesia.IndonesiaDictionary;
import com.doku.core.randomwordgenerator.indonesia.IndonesiaRandomWordGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;



@ExtendWith(MockitoExtension.class)
class IndonesiaRiddleGeneratorTest {

    @Mock
    private IndonesiaRandomWordGenerator indonesiaRandomWordGenerator;

    @Mock
    private IndonesiaDictionary indonesiaDictionary;

    @InjectMocks
    private IndonesiaRiddleGenerator indonesiaRiddleGenerator;

    @BeforeEach
    void setUp() {
        indonesiaRiddleGenerator = new IndonesiaRiddleGenerator();
    }

    @Test
    void testGenerateRandomWordMethod() throws Exception {
        when(indonesiaRandomWordGenerator.generate()).thenReturn("test");
        indonesiaRiddleGenerator.setRandomWordGenerator(indonesiaRandomWordGenerator);

        String result = indonesiaRiddleGenerator.produceRandomWord();
        assertEquals("test", result);
        verify(indonesiaRandomWordGenerator).generate();
    }

    @Test
    void testCheckDictionaryMethod() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        when(indonesiaDictionary.check("test")).thenReturn(objectNode);
        indonesiaRiddleGenerator.setDictionary(indonesiaDictionary);

        ObjectNode result = indonesiaRiddleGenerator.checkDictionary("test");
        assertEquals(objectNode, result);
        verify(indonesiaDictionary).check("test");
    }
}