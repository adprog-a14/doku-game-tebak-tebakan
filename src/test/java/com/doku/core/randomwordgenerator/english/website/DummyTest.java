package com.doku.core.randomwordgenerator.english.website;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.doku.core.randomwordgenerator.english.database.EnglishDatabase;
import org.junit.jupiter.api.Test;


public class DummyTest {

    @Test
    void testDummyClass() {
        EnglishDatabase englishDatabase = new EnglishDatabase();
        String result = englishDatabase.generate();
        assertEquals(null, result);
    }
}
