package com.doku.core.randomwordgenerator.english.website;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.doku.core.riddlegenerator.FailRetrievingRiddleException;
import java.io.IOException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class MerriamWebsterComTest {

    private MerriamWebsterCom merriamWebsterCom;

    @BeforeEach
    void setUp() {
        merriamWebsterCom = new MerriamWebsterCom();
    }

    @Test
    void testKbbiGenerateMethodSuccess() throws Exception {
        String result = merriamWebsterCom.generate();
        assertEquals(result.getClass().getName(), "java.lang.String");
    }

    @Test
    void testMerriamWebsterGenerateMethodFailWithIoException() throws Exception {
        MerriamWebsterCom merriamWebsterCom = mock(
                MerriamWebsterCom.class, Mockito.CALLS_REAL_METHODS);
        String mockUrl = "https://www.merriam-webster.com/browse/thesaurus/c";
        when(merriamWebsterCom.randomMeriamWebsterPage()).thenReturn(mockUrl);
        when(merriamWebsterCom.getMeriamWebsterHtml(mockUrl)).thenThrow(IOException.class);

        try {
            merriamWebsterCom.generate();
        } catch (FailRetrievingRiddleException e) {
            assertEquals(
                    "com.doku.core.riddlegenerator.FailRetrievingRiddleException",
                    e.getClass().getName());
        }
    }

    @Test
    void testMerriamWebsterGenerateMethodFailWithIllegalArgumentException() throws Exception {
        MerriamWebsterCom merriamWebsterCom = mock(
                MerriamWebsterCom.class, Mockito.CALLS_REAL_METHODS);
        String mockUrl = "https://www.merriam-webster.com/browse/thesaurus/c";
        when(merriamWebsterCom.randomMeriamWebsterPage()).thenReturn(mockUrl);
        when(merriamWebsterCom.getMeriamWebsterHtml(mockUrl)).thenReturn("tes");

        try {
            merriamWebsterCom.generate();
        } catch (FailRetrievingRiddleException e) {
            assertEquals(
                    "com.doku.core.riddlegenerator.FailRetrievingRiddleException",
                    e.getClass().getName());
        }
    }

}