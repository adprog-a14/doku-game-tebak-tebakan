package com.doku.core.randomwordgenerator.indonesia.website;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.doku.core.randomwordgenerator.indonesia.database.IndonesiaDatabase;
import org.junit.jupiter.api.Test;


public class DummyTest {

    @Test
    void dummyTest() {
        IndonesiaDatabase indonesiaDatabase = new IndonesiaDatabase();
        String result = indonesiaDatabase.generate();
        assertEquals(null, result);
    }
}
