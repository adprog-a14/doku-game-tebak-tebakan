package com.doku.core.randomwordgenerator.indonesia.website;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.doku.core.riddlegenerator.FailRetrievingRiddleException;
import java.io.IOException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class KbbiCoIdTest {

    private KbbiCoId kbbiCoId;

    @BeforeEach
    void setUp() {
        kbbiCoId = new KbbiCoId();
    }

    @Test
    void testKbbiGenerateMethodSuccess() throws Exception {
        String result = kbbiCoId.generate();
        assertEquals(result.getClass().getName(), "java.lang.String");
    }

    @Test
    void testKbbiGenerateMethodFailWithIllegalArgumentException() {
        KbbiCoId kbbiCoId = mock(KbbiCoId.class, Mockito.CALLS_REAL_METHODS);
        when(kbbiCoId.randomKbbiPage()).thenThrow(IllegalArgumentException.class);

        try {
            kbbiCoId.generate();
        } catch (FailRetrievingRiddleException e) {
            assertEquals(
                    "com.doku.core.riddlegenerator.FailRetrievingRiddleException",
                    e.getClass().getName());
        }
    }

    @Test
    void testKbbiGenerateMethodFailWithIoException() throws IOException {
        KbbiCoId kbbiCoId = mock(KbbiCoId.class, Mockito.CALLS_REAL_METHODS);
        when(kbbiCoId.randomKbbiPage()).thenReturn("https://kbbi.co.id/daftar-kata");
        when(kbbiCoId.getKbbiHtml("https://kbbi.co.id/daftar-kata")).thenThrow(IOException.class);

        try {
            kbbiCoId.generate();
        } catch (FailRetrievingRiddleException e) {
            assertEquals(
                    "com.doku.core.riddlegenerator.FailRetrievingRiddleException",
                    e.getClass().getName());
        }
    }

    @Test
    void testCreateRandomPageMethodWithInputOne() {
        String actual = kbbiCoId.createRandomKbbiRandomPage(1);
        assertEquals("https://kbbi.co.id/daftar-kata", actual);
    }

    @Test
    void testCreateRandomPageMethodWithInputMoreThanOne() {
        String actual = kbbiCoId.createRandomKbbiRandomPage(2);
        assertEquals("https://kbbi.co.id/daftar-kata?page=2", actual);
    }
}