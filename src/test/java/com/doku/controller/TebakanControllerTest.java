package com.doku.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.doku.service.TebakanService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;


@WebMvcTest(controllers = TebakanController.class)
class TebakanControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TebakanService tebakanService;

    @InjectMocks
    private TebakanController tebakanController;

    @BeforeEach
    void setUp() {
        tebakanController = new TebakanController();
    }

    @Test
    void testStartGameMethod() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode objectNode = mapper.createObjectNode();
        when(tebakanService.generateRiddle("Indonesia")).thenReturn(objectNode);
        MvcResult result = mvc.perform(
                get("/game-tebakan?language=Indonesia")
        ).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        assertEquals(content, mapper.writeValueAsString(objectNode));
    }

    @Test
    void testCheckAnswerMethod() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        when(tebakanService.checkAnswer("test1", "test")).thenReturn(true);
        MvcResult result = mvc.perform(
                post("/game-tebakan/cek?answer=test&encryptedAnswer=test1")
        ).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        assertEquals("true", content);
    }
}